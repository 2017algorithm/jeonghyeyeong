#include    <stdio.h>
#include    <queue>
#pragma warning(disable : 4996)
 
static std::queue<int> q;
static int BUTTON[1111111];
static int VISIT[1111111];
static int S, G, F, U, D;
 
void bfs()
{
    while (!q.empty())
    {
        int f = q.front();
        q.pop();
        if (f==G)
            return;
        if (f + U <= F && VISIT[f + U] == 0)
        {
            BUTTON[f + U] = BUTTON[f] + 1;
            q.push(f + U);
            VISIT[f + U] = 1;
        }
        if (f - D >= 1 && VISIT[f - D] == 0)
        {
            BUTTON[f - D] = BUTTON[f] + 1;
            q.push(f - D);
            VISIT[f - D] = 1;
        }
    }
}
int main()
{
    scanf("%d %d %d %d %d", &F, &S, &G, &U, &D);
    if (S == G)
    {
        printf("%d\n",0);
        return 0;
    }
    VISIT[S] = 1;
    BUTTON[S] = 0;
    q.push(S);
    bfs();
    if (VISIT[G] == 0)
        printf("use the stairs\n");
    else
        printf("%d\n", BUTTON[G]);
 
    return 0;
}